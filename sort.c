#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void swap(char** this, char** that)
{
	char* temp = *this;
	*this = *that;
	*that = temp;
}

int main(void)
{
	char* array[] = {"-c10", "*a10", "-a10", "-a1", "-a2", "=a3", "-b2", "-b100", "-b23", "-b99"};

	int swapped = 1;
	int i;

	printf("sorting array: \n");
	for(i=0; i < 10; i++)
	{
		printf("'%s' ", array[i]);
	}

	printf("\narray sorted:\n");
	do
	{
		swapped = 0;
		for(i=0; i < 9; i++)
		{
			if(strcmp(array[i], array[i+1]) > 0)
            {
                swap(&array[i], &array[i+1]);
                swapped = 1;
            }
		}
	}
	while(swapped);

	swapped = 1;
	
	do
	{
		swapped = 0;
		for(i=0; i < 9; i++)
		{
			if(strncmp(array[i], array[i+1], 2) == 0)
            {
            	if(atoi(array[i] + 2) > atoi(array[i+1] + 2))
            	{
            		swap(&array[i], &array[i+1]);
                	swapped = 1;
            	}
            }
		}
	}
	while(swapped);

	for(i=0; i < 10; i++)
	{
		printf("'%s' ", array[i]);
	}


	return 0;
}